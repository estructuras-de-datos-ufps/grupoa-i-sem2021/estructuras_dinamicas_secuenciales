/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.ufps.colecciones_seed;

/**
 * Pila implementada usando una ListaCD
 *
 * @author madarme
 */
public class Pila<T> {

    private ListaCD<T> myLista = new ListaCD();

    public Pila() {
    }

    public void apilar(T info) {
        this.myLista.insertarInicio(info);
    }

    public T desaPilar() {
        if (this.myLista.esVacia()) {
            throw new RuntimeException("Pila vacía , no se puede desapilar");
        }
        return this.myLista.eliminar(0);
    }

    public boolean esVacio() {
        return this.myLista.esVacia();
    }

    public int getTamanio() {
        return this.myLista.getTamanio();
    }
}
