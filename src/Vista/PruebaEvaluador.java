/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.EvaluadorExpresion;
import java.util.Scanner;

/**
 *
 * @author Invitado
 */
public class PruebaEvaluador {
    public static void main(String[] args) {
        
        System.out.println("Digite expresión:");
       Scanner in = new Scanner(System.in);
       String exp=in.nextLine();
       EvaluadorExpresion evaluador=new EvaluadorExpresion(exp);
       if(evaluador.esBalanceadoParentesis())
            System.out.println("Tiene paréntesis balanceado :)");
       else
            System.out.println("NO Tiene paréntesis balanceado :(");
       
    }
}
