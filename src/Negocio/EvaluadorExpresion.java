/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import util.ufps.colecciones_seed.Pila;

/**
 *
 * @author Invitado
 */
public class EvaluadorExpresion {
    
    private String expresion;

    public EvaluadorExpresion(String expresion) {
        this.expresion = expresion;
    }
    
    
    public boolean esBalanceadoParentesis()
    {
    
        if(this.expresion.isEmpty())
            throw new RuntimeException("no se puede realizar proceso de comprobación");
        //Pila de comprobación:
        Pila<Character> p=new Pila();
        for (int i = 0; i < this.expresion.length(); i++) {
            char letra = this.expresion.charAt(i);
            if (letra == '(') {
                p.apilar(letra);
            } else {
                if (letra == ')') {
                    if (p.esVacio()) {
                        return false;
                    }
                    p.desaPilar();
                }

            }
        }
        
      return p.esVacio();
    }
    
    
    /**
     *  1.{}
     *  2- []
     *  3. ()
     *  Ejemplo: [(r+e)] --> true
     *          ([(r+e)]) --> false --> por jerarquía
     * SOLO SE PUEDE USAR PILAS Y/O COLAS
     * @return Verdadero si tiene los signos de agrupamiento balanceados en ORDEN DE JERARQUÍA , o false en caso contrario
     */
    public boolean esBalanceadoSignos_Agrupacion()
    {
        // :)
        return false;
    }
    
    
    public String getExpresion() {
        return expresion;
    }

    public void setExpresion(String expresion) {
        this.expresion = expresion;
    }

    @Override
    public String toString() {
        return "EvaluadorExpresion{" + "expresion=" + expresion + '}';
    }
    
    
    
}
